---
title: What is the SSH Open Marketplace?
toc: true
navigationMenu:
  title: About the service
  position: 1
---

The Social Sciences and Humanities Open Marketplace, built as part of the
[Social Sciences and Humanities Open Cloud project (SSHOC)](https://www.sshopencloud.eu/), is a
discovery portal which pools and contextualises resources for Social Sciences and Humanities
research communities: **tools, services, training materials, datasets, publications and workflows**.
The Marketplace highlights and showcases solutions and research practices for every step of the SSH
research data life cycle.

The SSH Open Marketplace is:

- a **discovery portal**, to foster serendipity in digital methods
- an **aggregator** of useful and well curated resources
- a **catalogue**, contextualising resources
- an **entry point in the EOSC** for the Social Sciences and Humanities researchers

The SSH Open Marketplace is _not_:

- a repository. Nothing is hosted in the SSH Open Marketplace. Workflow content type can be hosted,
  but this is an exception
- a data catalogue. The goal is not to collect all the SSH datasets, but selected datasets are
  indexed to support the contextualisation (dataset mentioned in a publication or used in a training
  material for example).
- a commercial Marketplace. There is nothing to sell in the SSH Open Marketplace. Commercial
  software/services can be referenced.

Three key aspects have been guiding the development of the SSH Open Marketplace.

- **Curation** – This portal thrives on a curation process that makes it easy to discover the most
  appropriate results for each request, so that researchers can discover the best resources for the
  digital aspects of their work. The curation process relies on three components: automatic ingest
  and update of data sources; continuous curation of the information by the editorial team and –
  most important – contributions from users, the SSH research community.
- **Community** – The content available in the SSH Open Marketplace and its contextualisation is the
  result of collaborative work that is characterised by a user-centric approach. Features that allow
  contributions, feedback and comments are implemented – or will be soon – to ensure that the portal
  mirrors real research practices.
- **Contextualisation** – The portal puts all items into context: each solution suggested is linked
  to other related resources (e.g. a tutorial showing how to use a tool, a tool used in a workflow,
  a publication presenting research results produced using a given service). This contextualisation
  enhances the usefulness of the SSH Open Marketplace and ensures users receive the maximum possible
  benefit from all its contents.

## A Thematic Contribution to the European Open Science Cloud

The creation of the SSH Open Marketplace was funded by the
[Social Sciences and Humanities Open Cloud (SSHOC) project](https://cordis.europa.eu/project/id/823782)
which supported the integration and consolidation of thematic e-infrastructure platforms in
preparation for connecting them to the
[European Open Science Cloud (EOSC)](https://sshopencloud.eu/sshoc-eosc). The overall objective of
the SSHOC project was to realise the Social Sciences and Humanities part of EOSC.

As a domain-oriented discovery portal and the aggregator of the SSHOC project, the SSH Open
Marketplace, contributes directly to the EOSC, supplementing existing services such as the EOSC
Catalogue & Marketplace, and facilitating the fluid exchange of tools, services, data, and
knowledge.

![33 Key Exploitable Results of the SSHOC project](/assets/cms/images/33sshockers.png)

_Source:
[SSHOC project website](https://www.sshopencloud.eu/news/33-shocing-key-exploitable-results-support-your-research-ssh-and-beyond)_

## Governance & sustainability

After the end of the SSHOC project, 3 ERICs and some of their national nodes will ensure the
sustainability of the SSH Open Marketplace. They act as a **Governing Board** for the SSH Open
Marketplace and define the Marketplace strategic policy with regards to scientific, technical and
managerial matters.

<div style={{ display: 'grid', gap: 'var(--space-4)', gridTemplateColumns: 'repeat(3, 1fr)' }}>

[![DARIAH](/assets/cms/images/dariah-eu-logo.png)](https://dariah.eu)

[![CESSDA](/assets/cms/images/cessda-logo.png)](https://www.cessda.eu)

[![CLARIN](/assets/cms/images/clarin-logo.png)](https://clarin.eu)

</div>

An **[Editorial Team](/about/team)** ensures the day-to-day maintenance and the data quality of the
SSH Open Marketplace. The Editorial Team, liaising with the service providers and the end-users of
the service (SSH researchers and support staff for researchers) ensures the technical running of
operation, the effectiveness of the curation process and the editorial policy's successful
implementation.

## Main functionalities

Using the SSH Open Marketplace, you can:

- Search the resources, refining your query through faceted search.
  ![screenshot search resources](/assets/cms/images/search-refine-screenshot.png)
- Browse the content by activities or keywords
  ![browsing screenshot](/assets/cms/images/browse_activities_screenshot.png)
- Obtain a detailed view of every item registered in the website, including a structured set of
  metadata describing the resource, and highlighting the related items to facilitate the discovery
  of relevant resources
  ![detailed view screenshot](/assets/cms/images/details-related-screenshot.jpg)
- Contribute to the Marketplace by suggesting new content or enriching existing items. See the
  [Contribute pages](/contribute/overview) for all the details.
- Re-use the SSH Open Marketplace content using the Application programming Interface (API). See the
  API documentation in the ["technical aspects" section](/about/implementation), if you want to know
  more.

## Want to know more?

To know all the details that guided the development choices of the SSH Open Marketplace, you can
consult the following reports.

- Laure Barbot, Yoan Moranville, Frank Fischer, Clara Petitfils, Matej Ďurčo, Klaus Illmayer, Tomasz
  Parkoła, Philipp Wieder, & Sotiris Karampatakis. (2019). SSHOC D7.1 System Specification - SSH
  Open Marketplace (1.0). Zenodo.
  [https://doi.org/10.5281/zenodo.3547649](https://doi.org/10.5281/zenodo.3547649)
- Matej Ďurčo, Laure Barbot, Klaus Illmayer, Sotiris Karampatakis, Frank Fischer, Yoann Moranville,
  Joshua Tetteh Ocansey, Stefan Probst, Michał Kozak, Stefan Buddenbohm, & Seung-Bin Yim. (2021).
  7.2 Marketplace – Implementation (v1.0). Zenodo.
  [https://doi.org/10.5281/zenodo.5749465](https://doi.org/10.5281/zenodo.5749465)
- Deliverable 7.3 Marketplace Interoperability will be added soon
- Deliverable 7.4 Marketplace Data Population & Curation will be added soon
- Clara Petitfils, Suzanne Dumouchel, Nicolas Larrousse, Edward J. Gray, Laure Barbot, Arnaud Roi,
  Matej Ďurčo, Klaus Illmayer, Stefan Buddenbohm, & Tomasz Parkola. (2021). D7.5 Marketplace -
  Governance. [https://doi.org/10.5281/zenodo.5608487](https://doi.org/10.5281/zenodo.5608487)
- MS44 report Marketplace final release will be added soon

Several consultations and presentations of the SSH Open Marketplace have been given in recent years,
some examples and references:

- Frank Fischer, & Matej Durco. (2021, April 16). DARIAH Public Demonstration of the SSH Open
  Marketplace. Zenodo.
  [https://doi.org/10.5281/zenodo.4696322](https://doi.org/10.5281/zenodo.4696322)
- Buddenbohm, Stefan, Barbot, Laure, Gray, Edward, Willems, Marieke, Larrousse, Nicolas, &
  Petitfils, Clara. (2021). Requesting Crowd Expertise: The SSH Open Marketplace and LIBER. Zenodo.
  [https://doi.org/10.5281/zenodo.4757290](https://doi.org/10.5281/zenodo.4757290)
- Laure Barbot, & Edward Gray. (2021, September 29). Sharing digital tools in context: the SSH Open
  Marketplace. Zenodo.
  [https://doi.org/10.5281/zenodo.5535742](https://doi.org/10.5281/zenodo.5535742)
- Laure Barbot, Tracey Biller, Daan Broeder, Ron Dekker, Matej Durco, Irene Vipavc, Marieke Willems.
  Agile development of the SSH Open Marketplace: User Workshop. ITM Web Conf. 33 04001 (2020). DOI:
  10.1051/itmconf/20203304001
- Buddenbohm, Stefan. (2020). User Stories for the SSH Open Marketplace. Zenodo.
  [https://doi.org/10.5281/zenodo.4312630](https://doi.org/10.5281/zenodo.4312630)

If you want to promote the SSH Open Marketplace as part of your own work, do not hesitate to reuse
some of these materials, and to [let the team](/about/team) know about it, so that your work can
also be referenced in the list above.
